The Iji on ENIGMA project.

This repository contains the Iji original source files from:
http://www.remar.se/daniel/resources.php

However, they are saved in the ENIGMA project file format. 
However x2, the ENIGMA zip has been expanded to allow for easier revision control.

The easiest way to get up and running is as follows:
  1) Install ENIGMA: http://enigma-dev.org/
  2) Select *everything* in the iji_enigma directory and zip it into a single file (using "zip" compression) with the extension .egm.
  3) Open this file in ENIGMA, but make *sure* you do it from the iji_enigma directory (since it needs access to the "music" folder).

Optionally:
  1) You can remove the "music" folder from the egm zip to save space.

